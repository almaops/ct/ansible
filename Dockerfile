FROM ubuntu:22.04
ARG ANSIBLE_VERSION
ARG ANSIBLE_CORE_VERSION
ARG ANSIBLE_LINT_VERSION
RUN apt-get update -y && \
    apt-get install -y openssh-client python3 python3-pip git && \
    apt-get clean -y
RUN mkdir -p ~/.ssh && \
    echo "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
RUN pip3 install ansible==${ANSIBLE_VERSION} ansible-core==${ANSIBLE_CORE_VERSION} ansible-lint==${ANSIBLE_LINT_VERSION}
